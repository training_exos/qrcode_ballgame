import React from 'react';
// import { Button, StyleSheet, Text,  } from 'react-native';

import { createStackNavigator, createAppContainer } from "react-navigation";
import { createBottomTabNavigator, BottomTabBar } from 'react-navigation-tabs';
import EncodeScreen from './Components/Encode.js';
import DecodeScreen from './Components/Decode.js';
import HistoryScreen from './Components/History.js';
import BallGame from './Components/BallGame.js';

import { Provider } from "react-redux";
import store from "./store/index";

import index_store from "./index_store";



const AppNavigator = createBottomTabNavigator(
    {
        Encode: EncodeScreen,
        Decode: DecodeScreen,
        History: HistoryScreen,
        BallGame: BallGame
},
    {
        initialRouteName: "BallGame"
    }
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
              <AppContainer />
            </Provider>
        );
  }
}
