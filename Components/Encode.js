
import React from 'react';
import QRCode from 'react-native-qrcode';

import { StyleSheet, View, KeyboardAvoidingView,
         TextInput, TouchableOpacity, Text, Button} from 'react-native';

// import * as serviceWorker from '../serviceWorker.js';
import { connect } from 'react-redux';
import { addEncoded, setInput, setQrCode } from "../actions/index";

// Code pour générer des qrcode pris içi :
// https://aboutreact.com/generation-of-qr-code-in-react-native/ puis
// adapté pour fonctioner avec React redux

const mapDispatchToProps = (dispatch) => {
    return {
        addEncoded: (payload) => dispatch(addEncoded(payload)),
        setInput: (payload) => dispatch(setInput(payload)),
        setQrCode: (payload) => dispatch(setQrCode(payload)),
    };
};

const mapStateToProps = (state, ownProps) => {
    return {
        props: ownProps,
	state: state
    };
};

const EncodeScreenConnect = (props) => {
    
    return (
	// https://stackoverflow.com/questions/51857389/keyboardavoidingview-not-working-with-expo
	<KeyboardAvoidingView style={{ flex: 1 }} behavior={"padding"} >
	  <View style={
              { flex: 1, alignItems: "center", justifyContent: "center" }
          }>

            <QRCode value={props.state.app.valueForQRCode} //Setting the value of QRCode
                    size={250}  //Size of QRCode
                    bgColor="#000" //Backgroun Color of QRCode
                    fgColor="#fff" //Front Color of QRCode
            />

            <Text style={{textAlign: "left"}}>Encode Screen</Text>

            <TextInput  // Input to get the value to set on QRCode
                        style={styles.TextInputStyle}
                        onChangeText={(text) => props.setInput(text)}
                        underlineColorAndroid="transparent"
                        placeholder="Enter text to Generate QR Code"
            />
            <TouchableOpacity onPress={() => props.setQrCode(props.state.app.inputValue)}
                              activeOpacity={0.7}
                              style={styles.button}>
              <Text style={styles.TextStyle}> Generate QR Code </Text>
            </TouchableOpacity>

          </View>
        </KeyboardAvoidingView >
    );
    /* } */
}

const styles = StyleSheet.create({
    container: {
	flex: 1,
	backgroundColor: '#fff',
	alignItems: 'center',
	justifyContent: 'center',
    },
});


const EncodeScreen =
      connect(mapStateToProps, mapDispatchToProps)(EncodeScreenConnect);

export default EncodeScreen;
